from struct import *

def ushort_uint(number):
	return unpack(">HI", number)

def buf2latin(buf):
	length = unpack(">H", buf[:2])
	return (length[0], buf[2:].decode("latin-1"))

def ascii2buf(*buffers):
	size = len(buffers)
	result = pack(">I", size)
	for buffer in buffers:
		result += pack(">H", len(buffer))
		result += buffer.encode("ascii")
	return bytearray(result)